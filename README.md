# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed
	

## Items
1. menu/ game view/ gameover/ restart or quit
2. basic rules
3. 6 kinds of platforms
4. 
   * BGM for playing
   * sound effects for
					    1) step on normal platform
						2) hurt by nails
						3) jumping
						4) eating capsule will change BGM
   * BGM for gameover
5. Ranklist with name and score by using firebase database (view it by selecting Rank in menu)
6. various images and text
7. 
   * By pressing Enter while not standing on any platforms, the player will shoot a fireball            downwards and if it hit on the platform, the platform will crashed and both of them will           disappear.
   * If the player was hurt by the nails from the ceiling, he or she will get a few seconds of          unbeatable time, that is, the player won't be hurt during this duration even though the player
     touch the nails.Moreover, during this duration the player will become flashing.
   * If the player was hurt by nails, there will be a flash of red light.
   * By pressing spacebar you can pause the game and press it again you may resume.
   * The speed wil become faster and faster and so does the BGM.
   * The platforms will become fewer and fewer.
   * I add an capsule every 20 floors, If your successfully eat it, you'll get few seconds that         your weight become lighter and the platforms move slower.Besides, during this time the             player will become flashing and the BGM will change.



## Functions
1.  
    * XXX.alpha = 0;
    * game.add.tween(XXX).to({ alpha: 1 }, 50, Phaser.Easing.Linear.None, true, 0, 1000, true);
    * tween.pause();
    * (To make flashing for both text and sprite)
2.  
    * weapon
    * (an attributes in phaser helps to make fireball attack)
3.  
    * game.paused;
    * (pause the entire game except for music)
4.  
    * for creating numerous platforms use an array to store different platform that created in          different time.
5.  
    * txt.inputEnabled = true;
    * txt.events.onInputUp.add(callback);
    * txt.events.onInputOver.add(onOver);
    * txt.events.onInputOut.add(onOut);
    * (By using these functions you can add event to text by using mouse)
6.  
    * Most of the remaining functions were introduced in lecture before.Others were too trivial
      , so I had written comments for them in my code.











