var game = new Phaser.Game(500, 500, Phaser.AUTO, 'canvas'); 

game.global = { life: 5, floor: 1, score: 0, prev: 0};
game.state.add('boot', bootState); 
game.state.add('load', loadState); 
game.state.add('menu', menuState); 
game.state.add('play', playState);  
game.state.add('rank', rankState); 
game.state.add('gameover', gameoverState); 
game.state.start('boot');