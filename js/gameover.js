var text_gameover;
var text_score;
var time_btn = 0;
var time_close = 0;
var success = false;
var leave;
var error;
var gameoverState = {
    preload: function(){
        game.load.image('progressBar','assets/progressBar.png');
        game.load.audio('gg', 'music/Gameoverwav.wav');
    },
    create: function(){
        var gameover = game.add.audio('gg');
        gameover.play();

        text_gameover = game.add.text(game.width/2, game.height/2, "Game over", {fill: '#ff0000', fontSize: '20px', align: 'center'});
        text_gameover.anchor.setTo(0.5, 0.5);
        document.getElementById('Name').style.visibility = 'visible';
        document.getElementById('submit_btn').style.visibility = 'visible';
        leave = game.add.text(game.width/2, game.height-25, 'Please Enter your name and submit', { font: '20px Arial', fill: '#ffffff' }); 
        leave.anchor.setTo(0.5, 0.5);
        leave.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        //text fade
        leave.alpha = 0;
        game.add.tween(leave).to( { alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);

        error = game.add.text(game.width/2, game.height/5,'Empty name is not allowed', { font: '20px Arial', fill: '#ffbb33' });
        error.anchor.setTo(0.5,0.5);
        error.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        error.alpha = 0;
        game.add.tween(error).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        error.setText("");

        text_score = game.add.text(game.width/2, game.height/5*2,'Your Score : B' + game.global.floor, { font: '20px Arial', fill: 'green' });
        text_score.anchor.setTo(0.5,0.5);
        text_score.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        text_score.alpha = 1;
        game.add.tween(text_score).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        

        var txt = document.getElementById('Name');
        var btn = document.getElementById('submit_btn');
        txt.style.position = "relative";
        var top = game.height/5*3;
        txt.style.top = Number(top)+'px';
        btn.style.position = "relative";
        btn.style.top = Number(top)+'px';
        console.log(txt.style.top);
    },
    update: function() {
        
        var txt = document.getElementById('Name');
        var btn = document.getElementById('submit_btn');
       
        btn.addEventListener('click', function(){
            if(txt.value!=""){
                var listRef = firebase.database().ref("ranklist");
                var myRef=listRef.push();
                var Data={Name: txt.value, Score: 1000000-game.global.floor};
                myRef.set(Data);
                txt.value="";
                success = true;
                //console.log(success);
                leave.setText("");
                error.setText("");
                text_score.setText("");
                var restart = game.add.text(game.width/2, game.height/5, 'Press Enter to restart', { font: '20px Arial', fill: '#44d9e6' }); 
                restart.anchor.setTo(0.5,0.5);
                restart.alpha = 0;
                game.add.tween(restart).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
                var backtomenu = game.add.text(game.width/2, game.height/5*4, 'Press Esc to leave', { font: '20px Arial', fill: '#44d9e6' }); 
                backtomenu.anchor.setTo(0.5,0.5);
                backtomenu.alpha = 0;
                game.add.tween(backtomenu).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 0, 1000, true);
                document.getElementById('Name').value = '';
                document.getElementById('Name').style.visibility = 'hidden';
                document.getElementById('submit_btn').style.visibility = 'hidden';
            }
            else if(!success){
                console.log(2);
                error.setText("Empty name is not allowed");
            }
            
        });
        //console.log
        if(success){
            //console.log(123);
            error.setText("");
            var EnterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            var EscKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
            EnterKey.onDown.add(this.start, this); 
            EscKey.onDown.add(this.menu, this);
        }
         
    },
    start: function() {
        game.state.start('play');
    },
    menu: function() {
        game.state.start('menu');
    }
}