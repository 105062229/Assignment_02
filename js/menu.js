var menuState = {
    addMenuOption: function(text, callback) {
        var optionStyle = { font: '30pt TheMinion', fill: 'white', align: 'left', stroke: 'rgba(0,0,0,0)', srokeThickness: 4};
        var txt = game.add.text(30, game.height/5*(this.optionCount+1), text, optionStyle);
        var onOver = function (target) {
          target.fill = "aqua";
          target.stroke = "rgba(200,200,200,0.5)";
        };
        var onOut = function (target) {
          target.fill = "white";
          target.stroke = "rgba(0,0,0,0)";
        };
        txt.stroke = "rgba(0,0,0,0";
        txt.strokeThickness = 4;
        txt.inputEnabled = true;
        txt.events.onInputUp.add(callback);
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.optionCount ++;
    },
    start: function() {
        game.state.start('play');
    },
    Rank: function() {
        game.state.start('rank');
    },
    create: function() {
        var img = game.add.image(0, 0, 'background');
        img.width = game.width;
        img.height = game.height;
        
        var nameLabel = game.add.text(game.width/2, 80, '小朋友下樓梯', { font: '50px Arial', fill: '#ffffff' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        nameLabel.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.optionCount = 1;

        this.addMenuOption('Start', this.start);
        this.addMenuOption('Rank', this.Rank);
    }
     
}